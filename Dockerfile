FROM justsong/one-api-en:latest

EXPOSE 3000
ENV PORT 3000

ENV GLOBAL_WEB_RATE_LIMIT=600
ENV GLOBAL_API_RATE_LIMIT=1800

RUN mkdir /data/logs
RUN chmod -R 777 /data

ENTRYPOINT ["/one-api"]
